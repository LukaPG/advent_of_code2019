pub fn calculate_total_fuel_requirement(masses: &Vec<u32>) -> u32 {
    masses
        .iter()
        .filter(|&mass| *mass > 7)
        .fold(0, |total, &current| total + ((current / 3) - 2))
}

pub fn rocket_equation(masses: &Vec<u32>) -> u32 {
    masses
        .iter()
        .filter(|&mass| *mass > 7)
        .fold(0, |total, &current| total + calculate_needed_mass(&current))
}

fn calculate_needed_mass(original_mass: &u32) -> u32 {
    if *original_mass <= 7 {
        return 0;
    }

    let fuel_mass = original_mass / 3 - 2;

    if fuel_mass > 7 {
        fuel_mass + calculate_needed_mass(&fuel_mass)
    } else {
        fuel_mass
    }
}

#[cfg(test)]
mod tests_no_rocket_equation {
    extern crate test_case;
    use super::*;
    use test_case::test_case;

    #[test]
    fn empty_input_vector() {
        let empty: Vec<u32> = Vec::new();

        let result = calculate_total_fuel_requirement(&empty);

        assert_eq!(result, 0);
    }

    #[test_case(42, 12 ; "When element is 42, expected result is 12")]
    #[test_case(43, 12 ; "When element is 43, expected result is 12")]
    #[test_case(6, 0 ; "When element is 6, expected result is 0")]
    #[test_case(7, 0 ; "When element is 7, expected result is 0")]
    #[test_case(3, 0 ; "When element less than 6, expected result is 0")]
    fn one_element_vector(element: u32, expected: u32) {
        let vector: Vec<u32> = vec![element];

        let result = calculate_total_fuel_requirement(&vector);

        assert_eq!(result, expected);
    }

    #[test]
    fn multiple_elements() {
        let vector = vec![10, 11, 15];

        let result = calculate_total_fuel_requirement(&vector);

        assert_eq!(result, 5);
    }
}

#[cfg(test)]
mod tests_rocket_equation {
    extern crate test_case;
    use super::*;
    use test_case::test_case;

    #[test]
    fn empty_vector_zero_mass() {
        let vector: Vec<u32> = Vec::new();

        let result = rocket_equation(&vector);

        assert_eq!(result, 0);
    }

    #[test_case(14, 2 ; "For part of mass 14, total fuel required is 2")]
    #[test_case(1969, 966 ; "For part of mass 1969, total fuel required is 966")]
    #[test_case(100756, 50346 ; "For part of mass 100756, total fuel required is 50346")]
    fn correctly_calculates_fuel_for_one_part(part_mass: u32, expected: u32) {
        let vector = vec![part_mass];

        let result = rocket_equation(&vector);

        assert_eq!(result, expected);
    }

    #[test]
    fn correctly_sums_up_results() {
        let vector = vec![14, 1969, 100756];

        let result = rocket_equation(&vector);

        assert_eq!(result, 51314);
    }
}
