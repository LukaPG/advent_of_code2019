mod day1;
mod day2;

use std::fs::File;
use std::io::{BufRead, BufReader, Error, ErrorKind, Read};

/// Reads file of integers into a vector of u32
fn read<R: Read>(io: R) -> Result<Vec<u32>, Error> {
    let br = BufReader::new(io);
    br.lines()
        .map(|line| line.and_then(|v| v.parse().map_err(|e| Error::new(ErrorKind::InvalidData, e))))
        .collect()
}

fn main() -> Result<(), Error> {
    let maybe_masses = read(File::open("src/input/day1.txt")?);

    if let Ok(masses) = maybe_masses {
        println!(
            "Day 1  result: {}",
            day1::calculate_total_fuel_requirement(&masses)
        );
        println!("Day 1a result: {}", day1::rocket_equation(&masses));
    } else {
        println!("Could not read day 1 input: {:?}", maybe_masses);
    }

    let maybe_code = read(File::open("src/input/day2.txt")?);

    if let Ok(code) = maybe_code {
        let mut first_part_code = code.to_vec();
        first_part_code[1] = 12;
        first_part_code[2] = 2;
        day2::int_computer(&mut first_part_code);
        println!("Day 2  result: {}", first_part_code[0]);

        let second_part_code = code.to_vec();
        let maybe_pair = day2::find_valid_input(&second_part_code, 19690720);
        if let Some(pair) = maybe_pair {
            let (noun, verb) = pair;
            println!("Day 2a result: ({}, {}), {}", noun, verb, 100 * noun + verb);
        } else {
            println!("No combination of noun and verb matches...");
        }

    // let mut second_exercise_code_copu = code.to_vec();
    } else {
        println!("Could not read day 2 input: {:?}", maybe_code);
    }

    Ok(())
}
