use std::cmp::min;
use std::convert::TryInto;

pub fn int_computer(memory: &mut Vec<u32>) {
    let mut instr_ptr: usize = 0;

    while memory[instr_ptr] != 99 {
        let opcode = memory[instr_ptr];
        let first_arg_ptr: usize = memory[instr_ptr + 1].try_into().unwrap();
        let second_arg_ptr: usize = memory[instr_ptr + 2].try_into().unwrap();
        let res_ptr: usize = memory[instr_ptr + 3].try_into().unwrap();

        if opcode == 1 {
            memory[res_ptr] = memory[first_arg_ptr] + memory[second_arg_ptr];
        } else if opcode == 2 {
            memory[res_ptr] = memory[first_arg_ptr] * memory[second_arg_ptr];
        }

        instr_ptr += 4;
    }
}

pub fn find_valid_input(memory: &Vec<u32>, expected_result: u32) -> Option<(u32, u32)> {
    let max_len = min(100, memory.len().try_into().unwrap()); // Make sure we dont generate pointers beyond the end
    for noun in 0..max_len {
        for verb in 0..max_len {
            let mut run_memory = memory.to_vec();
            run_memory[1] = noun;
            run_memory[2] = verb;

            int_computer(&mut run_memory);
            if run_memory[0] == expected_result {
                return Some((noun, verb));
            }
        }
    }
    None
}

#[cfg(test)]
mod tests_find_valid_input {
    use super::find_valid_input;

    #[test]
    fn simple() {
        let vector = vec![1, 2, 3, 3, 2, 3, 11, 0, 99, 30, 40, 50];
        let desired_output = 3500;
        let expected_noun = 9;
        let expected_verb = 10;

        let result = find_valid_input(&vector, desired_output);

        assert!(result.is_some());
        assert_eq!(result.unwrap(), (expected_noun, expected_verb));
    }
}

#[cfg(test)]
mod tests_int_computer {
    use super::*;

    #[test]
    fn one_command() {
        let mut vector = vec![1, 1, 2, 1, 99];
        let expected = vec![1, 3, 2, 1, 99];

        int_computer(&mut vector);

        assert_eq!(vector, expected);
    }

    #[test]
    fn aoc_example_1() {
        let mut vector = vec![1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50];
        let expected = vec![3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50];

        int_computer(&mut vector);

        assert_eq!(vector, expected);
    }

    #[test]
    fn aoc_example_2() {
        let mut vector = vec![1, 0, 0, 0, 99];
        let expected = vec![2, 0, 0, 0, 99];

        int_computer(&mut vector);

        assert_eq!(vector, expected);
    }

    #[test]
    fn aoc_example_3() {
        let mut vector = vec![2, 3, 0, 3, 99];
        let expected = vec![2, 3, 0, 6, 99];

        int_computer(&mut vector);

        assert_eq!(vector, expected);
    }

    #[test]
    fn aoc_example_4() {
        let mut vector = vec![2, 4, 4, 5, 99, 0];
        let expected = vec![2, 4, 4, 5, 99, 9801];

        int_computer(&mut vector);

        assert_eq!(vector, expected);
    }

    #[test]
    fn aoc_example_5() {
        let mut vector = vec![1, 1, 1, 4, 99, 5, 6, 0, 99];
        let expected = vec![30, 1, 1, 4, 2, 5, 6, 0, 99];

        int_computer(&mut vector);

        assert_eq!(vector, expected);
    }
}
